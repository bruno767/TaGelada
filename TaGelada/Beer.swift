//
//  Beer.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/15/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import Foundation
import CoreData

class Beer: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var detail: String
    @NSManaged var idealTemp: String
    @NSManaged var review: NSNumber
    @NSManaged var media: NSData
    @NSManaged var note: String
    @NSManaged var createAt: NSDate

    class func insertNewBeer(name: String,detail:String, idealTemp: String, review: NSNumber, media: UIImage, note: String) -> Beer{
        let newBeer : Beer  = NSEntityDescription.insertNewObjectForEntityForName("Beer", inManagedObjectContext: CoreDataHelper.getContext()) as! Beer
        
        newBeer.name = name
        newBeer.detail = detail
        newBeer.idealTemp = idealTemp
        newBeer.review = review
        newBeer.media = UIImagePNGRepresentation(media)!
        newBeer.note = note
        newBeer.createAt = NSDate()
        
        CoreDataHelper.saveManagedContext()
        
        return newBeer
    }
    
    class func fetchAllBeers() -> [Beer] {
        let fetchRequest = NSFetchRequest(entityName: "Beer")
        fetchRequest.returnsObjectsAsFaults = false;
        let sortDescriptor = NSSortDescriptor(key: "createAt", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let beers:[Beer]?
        
        do {
            beers = try (CoreDataHelper.getContext().executeFetchRequest(fetchRequest) as? [Beer])!
            return beers!
        } catch {
            return [Beer]()
        }
    }

    func getName() -> String{
        return self.name
    }
    
    func getReview() -> NSNumber{
        return self.review
    }
    
    func getMedia() -> UIImage{
        return UIImage(data: self.media)!
    }
    
    func getNote() -> String{
        return self.note
    }
    

}
