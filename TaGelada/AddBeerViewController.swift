//
//  AddBeerViewController.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/15/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit
import CoreData

class AddBeerViewController: UIViewController {

    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = Beer.insertNewBeer("duff",detail: "Duff3",idealTemp: "2", review: 5, media: UIImage(named: "cerveja0")!, note: "cerveja sucesso3!!!")
        _ = Beer.insertNewBeer("superBock",detail:"SuperBock3",idealTemp: "2", review: 5, media: UIImage(named: "cerveja1")!, note: "cerveja doidera3!!!")
        _ = Beer.insertNewBeer("leffe",detail:"leffe",idealTemp: "2", review: 5, media: UIImage(named: "cerveja2")!, note: "cerveja iradaaa!!!")
        let beers = Beer.fetchAllBeers()
        
        print(beers)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
