//
//  Grid.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/21/15.
//  Copyright © 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit

class Grid: NSObject {

    var id = NSNumber()
    var slots = [Float()]
    var createAt = NSDate()
    var updateAt = NSDate()
    
    class func newGrid(id: NSNumber, slot1:Float, slot2:Float, slot3:Float, slot4:Float, createAt:NSDate, updateAt:NSDate) -> Grid{
        let newGrid = Grid()
        
        newGrid.id = id
        newGrid.slots = [slot1,slot2,slot3,slot4,slot1,slot3]
        newGrid.createAt = createAt
        newGrid.updateAt = updateAt
        
        return newGrid
        
    }
    func updateGrid(slot1:Float, slot2:Float, slot3:Float, slot4:Float, updateAt:NSDate) -> Grid{
        let newGrid = Grid()
        
        newGrid.slots = [slot1,slot2,slot3,slot4]
        newGrid.updateAt = updateAt
        
        return newGrid
    }

}
