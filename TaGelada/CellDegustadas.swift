//
//  CellDegustadas.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/23/15.
//  Copyright © 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit

class CellDegustadas: UITableViewCell {
    
    @IBOutlet weak var viewBar: UIView!
    @IBOutlet weak var labelBeer: UILabel!
    @IBOutlet weak var imgBeer: UIImageView!

    func initTile(img: UIImage, name: String ){
        
        
        imgBeer.image = img
        labelBeer.text = name
        labelBeer.font = UIFont(name: "Gill Sans", size: 19)
        labelBeer.textColor = UIColor(red: 245 / 255, green: 245 / 255, blue: 173 / 255, alpha: 1)
        
        
    }
}
