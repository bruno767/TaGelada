//
//  BeersViewController.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/15/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit
//import SAParallaxViewControllerSwift

class BeersViewController : UIViewController {
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
//
//extension BeersViewController: UICollectionViewDataSource {
//    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
//        
//        if let cell = cell as? SAParallaxViewCell {
//            
//            for view in cell.containerView.accessoryView.subviews {
//                if let view = view as? UILabel {
//                    view.removeFromSuperview()
//                }
//            }
//            
//            let index = indexPath.row % 6
//            let imageName = String(format: "image%d", index + 1)
//            if let image = UIImage(named: imageName) {
//                cell.setImage(image)
//            }
//            let title = ["Girl with Room", "Beautiful sky", "Music Festival", "Fashion show", "Beautiful beach", "Pizza and beer"]
//            let label = UILabel(frame: cell.containerView.accessoryView.bounds)
//            label.textAlignment = .Center
//            label.text = title[index]
//            label.textColor = .whiteColor()
//            label.font = .systemFontOfSize(30)
//            cell.containerView.accessoryView.addSubview(label)
//        }
//        
//        return cell
//    }
//}

//extension BeersViewController: UICollectionViewDelegate {
//    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        super.collectionView(collectionView, didSelectItemAtIndexPath: indexPath)
//        
//        if let cells = collectionView.visibleCells() as? [SAParallaxViewCell] {
//            let containerView = SATransitionContainerView(frame: view.bounds)
//            containerView.setViews(cells: cells, view: view)
//            
//            let viewController = DetailViewController()
//            viewController.transitioningDelegate = self
//            viewController.trantisionContainerView = containerView
//            
////            self.presentViewController(viewController, animated: true, completion: nil)
//            self.navigationController?.pushViewController(viewController, animated: true)
//        }
//    }
//}
