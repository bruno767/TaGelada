//
//  ViewController.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/9/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit


class TaGeladaViewController: UICollectionViewController {
    
    let jsonUrl = "http://grid-temperature.herokuapp.com/grids/20.json"
//    let jsonUrl = "http://localhost:3000/grids/20.json"
    
    var grid = Grid()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)

        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.translucent = true

        self.title = "TáGelada"
        
        
        self.fetchTemp()
        let timer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "fetchTemp", userInfo: nil, repeats: true)
        timer.fire()
        //self.collectionView?.backgroundColor = UIColor.whiteColor()
//        self.view.addSubview(UIImageView(image: UIImage(named: "Background")!))
       self.collectionView?.backgroundColor = UIColor.clearColor()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return grid.slots.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: CellTemp = collectionView.dequeueReusableCellWithReuseIdentifier("cellTemp", forIndexPath: indexPath) as! CellTemp
        
        cell.initTile("cerveja3", currentValue: grid.slots[indexPath.row])
        
        return cell
    }
    
    
    //MARK: fetch Url = "http://grid-temperature.herokuapp.com/grids/"
    
    func fetchTemp(){
        
        let session = NSURLSession.sharedSession()
        let shotsUrl = NSURL(string: jsonUrl)
        
        let task = session.dataTaskWithURL(shotsUrl!) {
            (data, response, error) -> Void in
            
            do {
                
                if let data = data {
                    
                    let jsonData = try NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers ) as! NSDictionary
                    print(jsonData)
                    let id = jsonData.valueForKey("id") as! NSNumber
                    let slot1 = jsonData.valueForKey("slot1") as! Float
                    let slot2 = jsonData.valueForKey("slot2") as! Float
                    let slot3 = jsonData.valueForKey("slot3") as! Float
                    let slot4 = jsonData.valueForKey("slot4") as! Float
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let createAt = dateFormatter.dateFromString(jsonData.valueForKey("created_at") as! String)
                    let updateAt = dateFormatter.dateFromString(jsonData.valueForKey("updated_at") as! String)
                    
                    self.grid = Grid.newGrid(id, slot1: slot1, slot2: slot2, slot3: slot3, slot4: slot4, createAt: createAt!, updateAt: updateAt!)
                    
                    //Atualiza dados da collectionView
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        // code here
                        
                        self.collectionView?.reloadData()
                        
                    })
                    // print(self.grid?.updateAt)
                    
                } else {
                    print("Sem internet")
                }
                
            } catch _ {
                // Error
            }
            
        }
        
        
        task.resume()
    }
    
    //MARK: CollectionView Layout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let gap: CGFloat = 10
        let width = CGRectGetWidth(collectionView.bounds)
        let hcount = 2
        _ = CGFloat(hcount) + 1
        
        let kwidth = width - gap
        let keywidth = kwidth/CGFloat(hcount)
        
        return CGSize(width:keywidth - 20,height:keywidth - 30)
    }
    
}

