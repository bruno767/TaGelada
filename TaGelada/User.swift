//
//  User.swift
//
//
//  Created by Bruno Augusto Mendes Barreto Alves on 11/11/15.
//
//

import Foundation
import CoreData


class User: NSManagedObject {
    
    @NSManaged var name: String?
    @NSManaged var idFacebook: String?
    @NSManaged var media: NSData?
    @NSManaged var mediaUrl: String?
    @NSManaged var password: String?
    @NSManaged var title: String?
    @NSManaged var email: String?
    @NSManaged var beers: NSSet?
    
    class func insertNewUser(name: String,idFacebook: String?, mediaUrl: String?,media: UIImage?, email: String, password: String, title: String?) -> User{
        let newUser : User  = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: CoreDataHelper.getContext()) as! User
        
        newUser.name = name
        if let id = idFacebook{
            newUser.idFacebook = id
        }
        if let m = media{
            newUser.media = UIImagePNGRepresentation(m)
        }
        if let url = mediaUrl{
            newUser.mediaUrl = url
        }
        newUser.email = email
        newUser.password = password
        newUser.title = title
        CoreDataHelper.saveManagedContext()
        
        return newUser
    }
    class func fetchUserByEmail(email: String) -> User? {
        let fetchRequest = NSFetchRequest(entityName: "User")
        fetchRequest.returnsObjectsAsFaults = false;
        fetchRequest.predicate = NSPredicate(format: "email = %@", email)
        
        
        do {
            let users = try CoreDataHelper.getContext().executeFetchRequest(fetchRequest)
            if let users = users as? [User] {
                return users.first
            }
            return nil
        } catch {
            return nil
        }
    }

}
