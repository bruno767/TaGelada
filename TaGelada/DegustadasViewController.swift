//
//  DegustadasViewController.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/11/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit

class DegustadasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableDegustadas: StackTableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tableDegustadas.delegate = self
        tableDegustadas.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        self.tableDegustadas.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Beer.fetchAllBeers().count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableDegustadas.dequeueReusableCellWithIdentifier("cellDegustadas", forIndexPath: indexPath) as! CellDegustadas
        
        let beer = Beer.fetchAllBeers()[indexPath.row]
        cell.initTile(beer.getMedia(), name: beer.getName())
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
