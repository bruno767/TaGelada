//
//  BeerManager.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/15/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import Foundation
import CoreData


public class CoreDataHelper: NSObject {
    
    
    public class func getContext() -> NSManagedObjectContext {
        return (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext!
    }
    
    public class func saveManagedContext() {
        var error : NSError? = nil
        do {
            try getContext().save()
        } catch let error1 as NSError {
            error = error1
            NSLog("Unresolved error saving context \(error), \(error!.userInfo)")
            abort()
        }
    }
   
}
