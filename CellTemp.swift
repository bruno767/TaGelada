//
//  CellTemp.swift
//  TaGelada
//
//  Created by Bruno Augusto Mendes Barreto Alves on 9/10/15.
//  Copyright (c) 2015 Bruno Augusto Mendes Barreto Alves. All rights reserved.
//

import UIKit

class CellTemp: UICollectionViewCell {
    
    @IBOutlet weak var imgTileCell: UIImageView!
    @IBOutlet weak var thermometer: MAThermometer!
    @IBOutlet weak var imgBrand: UIImageView!
    @IBOutlet weak var text: UILabel!
    
    func initTile(imgName: String, currentValue: Float ){
       // imgBrand.frame = CGRectMake(20, 20, 60, 70)
        imgBrand.image = UIImage(named: imgName)
        //thermometer = MAThermometer(frame: CGRectMake(90, 20, 25, 70))
        
//        thermometer.minValue = 0
//        thermometer.maxValue = 70
//        thermometer.curValue = CGFloat(currentValue)
//        thermometer.arrayColors = [UIColor.blueColor(),UIColor.redColor()]

        
        //text = UILabel(frame: CGRectMake(80, 90, 55, 22))
        text.text = String(currentValue) + "C˚"
        text.font = UIFont(name: "Gill Sans", size: 15)
        text.textColor = UIColor(red: 84 / 255, green: 36 / 255, blue: 34 / 255, alpha: 1)
        
      //  self.addSubview(text)
      //  self.addSubview(imgBrand)
      //  self.addSubview(thermometer)
        
    }
}
